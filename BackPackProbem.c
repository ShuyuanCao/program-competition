#include <stdio.h>
#include <string.h>
#define max(x,y) (x>y?x:y)

int n = 4, W = 5;
int w[4] = {2, 1, 3, 2};
int v[4] = {3, 2, 4, 2};

/*
 *  Solution 1: 
 *      针对每个物品是否放入背包进行搜索
 *      从第i个物品开始挑选总重小于j的部分
 *      it will be better that i starts from 0, which can be used as index of an array.
 *      (Recursion)
 */       
int rec(int i, int j){
    int res;

    if(i == n){
        // no more good, because i is the index of the array and now i == n
        res = 0;
    }else if(j < w[i]){
        // remaining weight is less than the weight of the current good, so cannot choose
        // the current good and just ignore it.
        res = rec(i + 1, j);
    }else{
        // Have to try 2 options:
        //  1. not select the current good 
        //  2. select the current good
        res = max(rec(i + 1, j), rec(i + 1, j - w[i]) + v[i] ); 
    }

    return res;
}

/*
 * Solution 2:
 *      使用二维数组存储中间计算结果，以避免solution1里边出现的重复计算
 *
 */
int dp[4 + 1][5 + 1];

int rec2(int i, int j){
    
    // already calculated the value
    if(dp[i][j] >= 0){
        return dp[i][j];
    }

    int res;
    if(i == n){
        res = 0;
    }else if(j < w[i]){
        res = rec2(i + 1, j);
    }else{
        res = max(rec2(i + 1, j), rec2(i + 1, j - w[i]) + v[i]);
    }
    // store the res into 2D array
    return dp[i][j] = res;
}

int main(){
    // solution 1
    //printf("%d\n", rec(0, W));
    // solution 2
    memset(dp, -1, sizeof(dp)); 
    printf("%d\n", rec2(0, W));
    return 0;
}
