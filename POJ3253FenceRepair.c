#include "stdio.h"

typedef long long ll;

int N = 5;
int L[5] = {2, 1, 5, 4, 3};

void swap(int *p1, int *p2){
    int tmp;
    tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}

void solve(){
    ll ans = 0;
    
    while(N > 1){
        
        int mii1 = 0, mii2 = 1;
        if(L[mii1] > L[mii2])swap(&mii1, &mii2);
        
        for(int i = 2; i < N; i++){
            if(L[i] < L[mii1]){
                mii2 = mii1;
                mii1 = i;
            }else if(L[i] < L[mii2]){
                mii2 = i;
            }
        }
        
        int t = L[mii1] + L[mii2];
        ans += t;
        
        printf("~~~~~~~~~~~~~~~~~~\n");
        printf("mii1: %d......\n", mii1);
        printf("mii2: %d......\n", mii2);
        printf("t is: %d......\n", t);
        printf("ans is: %lld......\n", ans);
        
        if(mii1 == N-1)swap(&mii1, &mii2);
        L[mii1] = t;
        L[mii2] = L[N-1];
        
        N--;
    }
}

int main(){
    printf("started......\n");
    solve();
    for(int i=0; i<5; i++){
        printf("array: %d\n", L[i]);
    }
    return 0;
}
